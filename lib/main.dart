import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:test_index_project/view/dashboard/mobile_ui/mobile_dashboard.dart';
import 'package:test_index_project/view/dashboard/web_ui/dashboard.dart';

Future<void> main() async {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: kIsWeb && MediaQuery.of(context).size.width > 600
          ? const WebDashboard()
          : const MobileDashboard(),
    );
  }
}
