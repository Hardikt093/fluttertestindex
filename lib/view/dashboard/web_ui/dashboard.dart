import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:material_segmented_control/material_segmented_control.dart';

import '../../utils/colors.dart';
import '../../utils/constants.dart';
import '../../utils/images.dart';

class WebDashboard extends StatefulWidget {
  const WebDashboard({super.key});

  @override
  State<WebDashboard> createState() => _WebDashboardState();
}

class _WebDashboardState extends State<WebDashboard> {
  int _currentSelection = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColorGlobal,
      body: ListView(
        children: [
          const HeaderWidget(),
          const SizedBox(
            height: 10,
          ),
          DefaultTabController(
              length: 3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  _tabBarView(),
                  _currentSelection == 0
                      ? const TabWidget(
                          oneDesc: tab1Text1,
                          twoDesc: tab1Text2,
                          threeDesc: tab1Text3,
                          oneImage: BaseImages.image3,
                          twoImage: BaseImages.image4,
                          threeImage: BaseImages.image2,
                        )
                      : _currentSelection == 1
                          ? const TabWidget(
                              oneDesc: tab2Text1,
                              twoDesc: tab2Text2,
                              threeDesc: tab2Text3,
                              oneImage: BaseImages.image3,
                              twoImage: BaseImages.image6,
                              threeImage: BaseImages.image7,
                            )
                          : const TabWidget(
                              oneDesc: tab3Text1,
                              twoDesc: tab3Text2,
                              threeDesc: tab3Text3,
                              oneImage: BaseImages.image3,
                              twoImage: BaseImages.image5,
                              threeImage: BaseImages.image8,
                            ),
                ],
              )),
        ],
      ),
    );
  }

  Widget _tabBarView() {
    return Center(
      child: SizedBox(
        height: 90,
        child: ScrollConfiguration(
          behavior: NoGlow(),
          child: MaterialSegmentedControl(
            children: _children,
            selectionIndex: _currentSelection,
            borderColor: lightCyanBlue,
            borderWidth: 1,
            selectedColor: skyBlueskyBlue,
            unselectedColor: Colors.white,
            selectedTextStyle: const TextStyle(
              color: lightCyan,
              letterSpacing: 0.84,
              fontSize: 14,
            ),
            unselectedTextStyle: const TextStyle(
              color: moderateCyan,
              letterSpacing: 0.84,
              fontSize: 14,
            ),
            borderRadius: 12.0,
            disabledChildren: _disabledIndices,
            verticalOffset: 12.0,
            horizontalPadding: const EdgeInsets.symmetric(horizontal: 20.0),
            onSegmentTapped: (index) {
              setState(() {
                _currentSelection = index;
              });
            },
          ),
        ),
      ),
    );
  }

// Holds all indices of children to be disabled.
  final List<int> _disabledIndices = [];

  final Map<int, Widget> _children = {
    0: const Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Text(
        tab1Text,
        textAlign: TextAlign.center,
      ),
    ),
    1: const Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Text(
        textAlign: TextAlign.center,
        tab2Text,
      ),
    ),
    2: const Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Text(
        textAlign: TextAlign.center,
        tab3Text,
      ),
    ),
  };
}

class NoGlow extends ScrollBehavior {
  @override
  Widget buildOverscrollIndicator(
      BuildContext context, Widget child, ScrollableDetails details) {
    return child;
  }
}

class HeaderWidget extends StatelessWidget {
  const HeaderWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.sizeOf(context).height * 0.7,
          child: ClipPath(
            clipper: WaveClipperTwo(),
            child: Container(
              decoration: const BoxDecoration(
                  gradient: LinearGradient(colors: [
                paleBlue,
                lightCyan,
              ])),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  //This is Registration button Widget
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        deineJobWebsiteText,
                        style: TextStyle(
                            fontSize: 42,
                            fontWeight: FontWeight.bold,
                            fontFamily: latoMedium,
                            letterSpacing: 1.26,
                            color: grayishBlue.withOpacity(1)),
                        textAlign: TextAlign.start,
                      ),
                      SizedBox(
                          height: MediaQuery.sizeOf(context).height * 0.08),
                      Container(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 30,
                        ),
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                          gradient: LinearGradient(
                            colors: [
                              moderateCyan,
                              darkSkyBlue,
                            ],
                          ),
                        ),
                        child: ElevatedButton(
                          onPressed: () {},
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.transparent,
                            shadowColor: Colors.transparent,
                          ),
                          child: const Text(
                            kostenlosRegistrierenText,
                            style: TextStyle(color: lightCyan),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(width: MediaQuery.sizeOf(context).width * 0.12),
                  //This is center Image Widget
                  Container(
                    decoration: const BoxDecoration(
                        shape: BoxShape.circle, color: whiteColorGlobal),
                    height: MediaQuery.sizeOf(context).width * 0.2,
                    width: MediaQuery.sizeOf(context).width * 0.2,
                    child: ClipOval(
                      clipBehavior: Clip.hardEdge,
                      child: SvgPicture.asset(
                        BaseImages.image1,
                      ),
                    ),
                  ),

                  SizedBox(width: MediaQuery.sizeOf(context).width * 0.18),
                ],
              ),
            ),
          ),
        ),
        //This is Login Button Header
        Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: whiteColorGlobal,
            boxShadow: [
              BoxShadow(
                color: silver.withOpacity(0.16),
                offset: const Offset(0, 3),
                blurRadius: 6,
              ),
            ],
            borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(12),
                bottomRight: Radius.circular(12)), // Rounded corners
          ),
          child: const Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: EdgeInsets.only(right: 17.0, top: 20, bottom: 20),
              child: Text(
                loginText,
                style: TextStyle(
                  fontSize: 18,
                  color: moderateCyan,
                  fontFamily: latoMedium,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class TabWidget extends StatelessWidget {
  final String? oneDesc;
  final String? twoDesc;
  final String? threeDesc;
  final String? oneImage;
  final String? twoImage;
  final String? threeImage;

  const TabWidget({
    this.oneDesc,
    this.twoDesc,
    this.threeDesc,
    this.oneImage,
    this.twoImage,
    this.threeImage,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.50,
            child: const Padding(
              padding: EdgeInsets.symmetric(vertical: 50),
              child: Text(
                tab1DescText,
                style: TextStyle(
                    fontSize: 40,
                    fontFamily: latoMedium,
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.26,
                    color: darkGrayishBlue),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Stack(
                alignment: AlignmentDirectional.centerStart,
                children: [
                  Container(
                    height: 200,
                    width: 200,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        color: aliceBlue),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      const Padding(
                        padding: EdgeInsets.only(left: 50, bottom: 0),
                        child: Text(
                          "1.",
                          style: TextStyle(
                            fontSize: 130,
                            fontFamily: latoMedium,
                            height: 0.1,
                            color: lightGrayishBlue,
                          ),
                        ),
                      ),
                      Text(
                        oneDesc ?? "",
                        style: const TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.w500,
                            fontFamily: latoMedium,
                            letterSpacing: 1.26,
                            color: lightGrayishBlue),
                        textAlign: TextAlign.center,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 50),
                        child: SvgPicture.asset(
                          oneImage!,
                          fit: BoxFit.scaleDown,
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ],
          ),
          const SizedBox(
            height: 50,
          ),
          Stack(
            alignment: Alignment.center,
            children: [
              Column(
                children: [
                  ClipPath(
                    clipper: WaveClipperTwo(reverse: true),
                    child: Container(
                      height: 160,
                      color: lightCyan,
                    ),
                  ),
                  ClipPath(
                    clipper: WaveClipperOne(),
                    child: Container(
                      height: 160,
                      color: lightCyan,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      SvgPicture.asset(
                        twoImage!,
                        fit: BoxFit.scaleDown,
                      ),
                      const Padding(
                        padding: EdgeInsets.only(left: 100, bottom: 0),
                        child: Text(
                          "2.",
                          style: TextStyle(
                            fontSize: 130,
                            fontFamily: latoMedium,
                            height: 0.1,
                            color: lightGrayishBlue,
                          ),
                        ),
                      ),
                      Text(
                        twoDesc ?? "",
                        style: const TextStyle(
                            fontSize: 30,
                            fontFamily: latoMedium,
                            fontWeight: FontWeight.w600,
                            letterSpacing: 1.26,
                            color: lightGrayishBlue),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ],
              )
            ],
          ),
          const SizedBox(
            height: 50,
          ),
          SizedBox(
            height: 200,
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Stack(
                  alignment: Alignment.topLeft,
                  children: [
                    Container(
                      height: 200,
                      width: 200,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                          color: aliceBlue),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Padding(
                          padding: EdgeInsets.only(left: 80, bottom: 0),
                          child: Text(
                            "3.",
                            style: TextStyle(
                              fontSize: 130,
                              fontFamily: latoMedium,
                              height: 0.1,
                              color: lightGrayishBlue,
                            ),
                          ),
                        ),
                        Text(
                          threeDesc ?? "",
                          style: const TextStyle(
                              fontSize: 30,
                              fontFamily: latoMedium,
                              letterSpacing: 1.26,
                              fontWeight: FontWeight.w600,
                              color: lightGrayishBlue),
                          textAlign: TextAlign.center,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 50),
                          child: SvgPicture.asset(
                            threeImage!,
                            fit: BoxFit.scaleDown,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 200,
          )
        ],
      ),
    );
  }
}
