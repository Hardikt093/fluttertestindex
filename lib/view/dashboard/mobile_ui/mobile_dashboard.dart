import 'package:flutter/material.dart';
import 'package:test_index_project/view/dashboard/mobile_ui/body_widget.dart';
import 'package:test_index_project/view/utils/colors.dart';
import 'package:test_index_project/view/utils/constants.dart';

class MobileDashboard extends StatefulWidget {
  const MobileDashboard({Key? key}) : super(key: key);

  @override
  State<MobileDashboard> createState() => _MobileDashboardState();
}

class _MobileDashboardState extends State<MobileDashboard> {
  ScrollController scrollController = ScrollController();
  bool showbtn = false;

  @override
  void initState() {
    scrollController.addListener(() {
      double showoffset = 10.0;

      if (scrollController.offset > showoffset) {
        showbtn = true;
        setState(() {});
      } else {
        showbtn = false;
        setState(() {});
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      bottomNavigationBar: BottomAppBar(
        color: whiteColorGlobal,
        child: Container(
          height: 80,
          decoration: const BoxDecoration(
            color: whiteColorGlobal,
            boxShadow: [
              BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 6),
            ],
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(12),
                topRight: Radius.circular(12)), // Rounded corners
          ), // Height of the bottom widget
          child: Center(
            child: SizedBox(
              width: 320,
              height: 40,
              child: DecoratedBox(
                decoration: BoxDecoration(
                  gradient:
                      const LinearGradient(colors: [moderateCyan, darkSkyBlue]),
                  borderRadius: BorderRadius.circular(12),
                ),
                child: ElevatedButton(
                  onPressed: () {
                    scrollController.animateTo(0,
                        duration: const Duration(milliseconds: 500),
                        //duration of scroll
                        curve: Curves.fastOutSlowIn //scroll type
                        );
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.transparent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12),
                    ),
                  ),
                  child: const Text(
                    kostenlosRegistrierenText,
                    style: TextStyle(
                      letterSpacing: 0.86,
                      fontSize: 17,
                      color: lightCyan,
                      fontFamily: latoMedium,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
      appBar: AppBar(
        backgroundColor: whiteColorGlobal,
        actions: const [
          Align(
            alignment: Alignment.center,
            child: Padding(
              padding: EdgeInsets.only(right: 17.0),
              child: Text(
                loginText,
                style: TextStyle(
                  fontSize: 18,
                  color: moderateCyan,
                  fontFamily: latoMedium,
                ),
              ),
            ),
          ),
        ],
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(12),
          ),
        ),
      ),
      body: SingleChildScrollView(
        controller: scrollController,
        physics: const BouncingScrollPhysics(),
        child: const BodyWidget(),
      ),
    );
  }
}
