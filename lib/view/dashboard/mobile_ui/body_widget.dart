import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_svg/svg.dart';
import 'package:material_segmented_control/material_segmented_control.dart';
import 'package:test_index_project/view/dashboard/web_ui/dashboard.dart';
import 'package:test_index_project/view/utils/colors.dart';
import 'package:test_index_project/view/utils/constants.dart';
import 'package:test_index_project/view/utils/images.dart';

class BodyWidget extends StatefulWidget {
  const BodyWidget({Key? key}) : super(key: key);

  @override
  State<BodyWidget> createState() => _BodyWidgetState();
}

class _BodyWidgetState extends State<BodyWidget> {
  int? _currentSelection = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _clipPath(),
        _tabBarView(),
        _titleOfTabBar(),
        const SizedBox(
          height: 20,
        ),
        Stack(
          children: [
            SizedBox(
              height: 265,
              child: Stack(
                children: [
                  Positioned(
                    left: -30,
                    top: 40,
                    child: Container(
                      height: 208,
                      width: 208,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: bg,
                          border: Border.all(width: 1, color: bg)),
                      child: const Center(
                        child: Text("1.",
                            style: TextStyle(
                                fontSize: 130,
                                fontFamily: latoMedium,
                                letterSpacing: 0,
                                color: lightGrayishBlue)),
                      ),
                    ),
                  ),
                  Align(
                    alignment: AlignmentDirectional.topEnd, // <-- SEE HERE
                    child: Padding(
                      padding: const EdgeInsets.only(right: 35.0),
                      child: SvgPicture.asset(
                        BaseImages.image3,
                      ),
                    ),
                  ),
                  Positioned(
                    right: 25,
                    top: 165,
                    child: Text(
                        _currentSelection == 0
                            ? tab1Text1
                            : _currentSelection == 1
                                ? tab2Text1Mobile
                                : tab3Text1Mobile,
                        textAlign: TextAlign.left,
                        style: const TextStyle(
                            fontSize: 16,
                            fontFamily: latoMedium,
                            letterSpacing: 0.47,
                            color: lightGrayishBlue)),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 208.0),
              child: Stack(
                children: [
                  Column(
                    children: [
                      ClipPath(
                        clipper: WaveClipperTwo(reverse: true),
                        child: Container(
                          height: 200,
                          decoration: const BoxDecoration(
                              gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            colors: [
                              lightCyan,
                              paleBlue,
                            ],
                          )),
                        ),
                      ),
                      ClipPath(
                        clipper: WaveClipperOne(),
                        child: Container(
                          height: 200,
                          decoration: const BoxDecoration(
                              gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            colors: [
                              lightCyan,
                              paleBlue,
                            ],
                          )),
                        ),
                      ),
                    ],
                  ),
                  Stack(
                    children: [
                      Align(
                        alignment: Alignment.bottomRight,
                        child: Padding(
                            padding: const EdgeInsets.only(top: 45.0, left: 45),
                            child: SizedBox(
                              height: 120,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  const Text(
                                    '2.',
                                    style: TextStyle(
                                        fontSize: 130,
                                        fontFamily: latoMedium,
                                        letterSpacing: 0,
                                        color: lightGrayishBlue),
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    _currentSelection == 0
                                        ? tab1Text2
                                        : _currentSelection == 1
                                            ? tab2Text2
                                            : tab3Text2Mobile,
                                    style: const TextStyle(
                                        fontSize: 16,
                                        fontFamily: latoMedium,
                                        letterSpacing: 0.47,
                                        color: lightGrayishBlue),
                                  )
                                ],
                              ),
                            )),
                      ),
                      Align(
                        alignment: AlignmentDirectional.topEnd, // <-- SEE HERE
                        child: Padding(
                          padding: EdgeInsets.only(
                            right: 45.0,
                            top: _currentSelection == 0
                                ? 195
                                : _currentSelection == 1
                                    ? 145
                                    : 175,
                          ),
                          child: SvgPicture.asset(
                            _currentSelection == 0
                                ? BaseImages.image4
                                : _currentSelection == 1
                                    ? BaseImages.image6
                                    : BaseImages.image5,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 550.0),
              child: Stack(
                children: [
                  SizedBox(
                    height: 400,
                    child: Stack(
                      children: [
                        Positioned(
                          left: -30,
                          top: 0,
                          child: Container(
                            height: 303,
                            width: 303,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: bg,
                                border: Border.all(
                                    width: 1,
                                    color: aliceBlue.withOpacity(0.5))),
                            child: const Align(
                              alignment: Alignment.topCenter,
                              child: Text('3.',
                                  style: TextStyle(
                                      fontSize: 130,
                                      fontFamily: latoMedium,
                                      letterSpacing: 0,
                                      color: lightGrayishBlue)),
                            ),
                          ),
                        ),
                        Align(
                          alignment: AlignmentDirectional.topEnd,
                          child: Padding(
                            padding:
                                const EdgeInsets.only(right: 30.0, top: 70),
                            child: Text(
                                _currentSelection == 0
                                    ? tab1Text3Mobile
                                    : _currentSelection == 1
                                        ? tab2Text3Mobile
                                        : tab3Text3Mobile,
                                textAlign: TextAlign.left,
                                style: const TextStyle(
                                    fontSize: 16,
                                    fontFamily: latoMedium,
                                    letterSpacing: 0.47,
                                    color: lightGrayishBlue)),
                          ),
                        ),
                        Positioned(
                          right: _currentSelection == 0
                              ? 15
                              : _currentSelection == 1
                                  ? 35
                                  : 40,
                          top: _currentSelection == 0
                              ? 120
                              : _currentSelection == 1
                                  ? 160
                                  : 170,
                          child: SvgPicture.asset(
                            _currentSelection == 0
                                ? BaseImages.image2
                                : _currentSelection == 1
                                    ? BaseImages.image7
                                    : BaseImages.image8,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _tabBarView() {
    return SizedBox(
      height: 90,
      child: ScrollConfiguration(
        behavior: NoGlow(),
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: [
            MaterialSegmentedControl(
              children: _children,
              selectionIndex: _currentSelection,
              borderColor: lightCyanBlue,
              borderWidth: 1,
              selectedColor: skyBlueskyBlue,
              unselectedColor: Colors.white,
              selectedTextStyle: const TextStyle(
                color: lightCyan,
                letterSpacing: 0.84,
                fontSize: 14,
              ),
              unselectedTextStyle: const TextStyle(
                color: moderateCyan,
                letterSpacing: 0.84,
                fontSize: 14,
              ),
              borderRadius: 12.0,
              disabledChildren: _disabledIndices,
              verticalOffset: 12.0,
              horizontalPadding: const EdgeInsets.symmetric(horizontal: 20.0),
              onSegmentTapped: (index) {
                setState(() {
                  _currentSelection = index;
                });
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _titleOfTabBar() {
    return Text(
      _currentSelection == 0
          ? tab1DescTextMobile
          : _currentSelection == 1
              ? tab2DescTextMobile
              : tab3DescTextMobile,
      textAlign: TextAlign.center,
      style: const TextStyle(
          fontSize: 21,
          fontFamily: latoMedium,
          letterSpacing: 1.26,
          color: darkGrayishBlue),
    );
  }

  final Map<int, Widget> _children = {
    0: const Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Text(
        tab1Text,
        textAlign: TextAlign.center,
      ),
    ),
    1: const Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Text(
        textAlign: TextAlign.center,
        tab2Text,
      ),
    ),
    2: const Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Text(
        textAlign: TextAlign.center,
        tab3Text,
      ),
    ),
  };

  final List<int> _disabledIndices = [];

  Widget _clipPath() {
    return ClipPath(
      clipper: WaveClipperTwo(),
      child: Container(
        height: 620.37,
        width: double.infinity,
        decoration: const BoxDecoration(
            gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [
            lightCyan,
            paleBlue,
          ],
        )),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(height: 100),
            Text(deineJobWebsiteTextMobile,
                style: TextStyle(
                    fontSize: 42,
                    fontFamily: latoMedium,
                    letterSpacing: 1.26,
                    color: grayishBlue.withOpacity(1)),
                textAlign: TextAlign.center),
            SvgPicture.asset(
              alignment: Alignment.topRight,
              BaseImages.image1,
            ),
          ],
        ),
      ),
    );
  }
}
