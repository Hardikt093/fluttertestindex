import 'package:flutter/material.dart';

const Color whiteColorGlobal = Color(0xFFFFFFFF);
const Color darkGrayishBlue = Color(0xFF4A5568);
const Color aliceBlue = Color(0xFFF7FAFC);
const Color lightGrayishBlue = Color(0xFF718096);
const Color lightCyan = Color(0xFFE6FFFA);
const Color paleBlue = Color(0xFFEBF4FF);
const Color grayishBlue = Color(0xFF2D3748);
const Color lightCyanBlue = Color(0xFFCBD5E0);
const Color skyBlueskyBlue = Color(0xFF81E6D9);
const Color moderateCyan = Color(0xFF319795);
const Color silver = Color(0xFF00000029);
const Color darkSkyBlue = Color(0xFF3182CE);
const Color silverShade = Color(0xFF00000033);
const Color darkSilver = Color(0xFF707070);
const Color bg = Color(0xFFF7FAFC);
