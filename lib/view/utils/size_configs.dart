import 'package:flutter/cupertino.dart';

class ScreenSize {
  static double screenSizeWidthPercentage(
      BuildContext context, double percentage) {
    return (MediaQuery.of(context).size.width / 100) * percentage;
  }

  static double screenSizeHeightPercentage(
      BuildContext context, double percentage) {
    return (MediaQuery.of(context).size.height / 100) * percentage;
  }
}

// Get the proportionate height as per screen size
double getProportionateScreenHeight(BuildContext context, double inputHeight) {
  double designHeight = 660.0;
  if (MediaQuery.of(context).orientation == Orientation.portrait) {
    return (inputHeight / designHeight) * MediaQuery.of(context).size.height;
  } else {
    return (inputHeight / designHeight) * MediaQuery.of(context).size.width;
  }
}

// Get the proportionate height as per screen size
double getProportionateScreenWidth(BuildContext context, double inputWidth) {
  double designWidth = 360;
  if (MediaQuery.of(context).orientation == Orientation.portrait) {
    return (inputWidth / designWidth) * MediaQuery.of(context).size.width;
  } else {
    return (inputWidth / designWidth) * MediaQuery.of(context).size.height;
  }
}

extension Capitalized on String {
  String capitalized() =>
      substring(0, 1).toUpperCase() + substring(1).toLowerCase();
}

extension NumberParsing on String {
  String toDouble() {
    return double.parse(this).toStringAsFixed(2);
  }
}
