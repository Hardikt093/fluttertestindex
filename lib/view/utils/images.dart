class BaseImages {
  static const String image1 = "assets/images/undraw_agreement_aajr.svg";
  static const String image2 = "assets/images/undraw_personal_file_222m.svg";
  static const String image3 = "assets/images/undraw_Profile_data_re_v81r.svg";
  static const String image4 = "assets/images/undraw_task_31wc.svg";
  static const String image5 = "assets/images/undraw_job_offers_kw5d.svg";
  static const String image6 = "assets/images/undraw_about_me_wa29.svg";
  static const String image7 = "assets/images/undraw_swipe_profiles1_i6mr.svg";
  static const String image8 = "assets/images/undraw_business_deal_cpi9.svg";
}
