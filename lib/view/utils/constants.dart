const latoMedium = 'Lato-Medium';
const loginText = "Login";
const deineJobWebsiteText = "Deine Job\nwebsite";
const kostenlosRegistrierenText = "Kostenlos Registrieren";

/// Tab 1
const tab1Text = "Arbeitnehmer";
const tab1DescText = "Drei einfache Schritte zu deinem neuen Job";
const tab1Text1 = "Erstellen dein Lebenslauf";
const tab1Text2 = "Erstellen dein Lebenslauf";
const tab1Text3 = "Mit nur einem Klick bewerben";

/// Tab 2
const tab2Text = "Arbeitnehmer";
const tab2DescText = "Drei einfache Schritte zu deinem neuen Mitarbeiter";
const tab2Text1 = "Erstellen dein Unternehmensprofil";
const tab2Text2 = "Erstellen ein Jobinserat";
const tab2Text3 = "Wähle deinen neuen Mitarbeiter aus";

/// Tab 3
const tab3Text = "Temporärbüro";
const tab3DescText = "Drei einfache Schritte zur Vermittlung neuer Mitarbeiter";
const tab3Text1 = "Erstellen dein Unternehmensprofil";
const tab3Text2 = "Erhalte Vermittlungs- angebot von Arbeitgeber";
const tab3Text3 = "Vermittlung nach Provision oder Stundenlohn";

///Mobile
const deineJobWebsiteTextMobile = "Deine Job\nwebsite";

/// Tab 1 Mobile
const tab1DescTextMobile = "Drei einfache Schritte\nzu deinem neuen Job";
const tab1Text3Mobile = "Mit nur einem Klick\nbewerben";

/// Tab 2 Mobile
const tab2DescTextMobile =
    "Drei einfache Schritte\nzu deinem neuen Mitarbeiter";
const tab2Text1Mobile = "Erstellen dein\nUnternehmensprofil";
const tab2Text3Mobile = "Wähle deinen neuen\nMitarbeiter aus";

/// Tab 3 Mobile
const tab3DescTextMobile =
    "Drei einfache Schritte zur\nVermittlung neuer Mitarbeiter";
const tab3Text1Mobile = "Erstellen dein\nUnternehmensprofil";
const tab3Text2Mobile = "Erhalte Vermittlungs-\nangebot von Arbeitgeber";
const tab3Text3Mobile = "Vermittlung nach\nProvision oder\nStundenlohn";
